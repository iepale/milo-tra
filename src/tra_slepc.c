/*------------ -------------- -------- --- ----- ---   --       -            -
 *  milonga transient problem solution routines copied from eigen_slepc.c
 *
 *  Copyright (C) 2010--2015 jeremy theler
 *
 *  This file is part of milonga.
 *
 *  milonga is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  milonga is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with wasora.  If not, see <http://www.gnu.org/licenses/>.
 *------------------- ------------  ----    --------  --     -       -         -
 */

//#include <slepceps.h>
//#include <petscksp.h>
#include <petscts.h>

#include "milonga.h"

int init_tra(void);
static PetscErrorCode f(TS ts,PetscReal t, Vec u ,Vec u_t ,Vec F, void *ctx);
static PetscErrorCode derecha(TS ts,PetscReal t, Vec u ,Vec F, void *ctx);
static PetscErrorCode fjac(TS ts,PetscReal t,Vec u ,Vec u_t,PetscReal shift,Mat A,Mat B, void *ctx );
static PetscErrorCode evaluate_matrices(TS ts,PetscReal stagetime );
static PetscErrorCode monitor(TS ts,PetscInt step,PetscReal time,Vec u,void *ctx);
int amano(void);
int amano2(void);
#undef  __FUNCT__
#define __FUNCT__ "init_tra"
int init_tra(void)
  {
  KSP ksp;
  KSPType kspname;
  SNES snes;
  SNESType snesname;
  Mat A;  //Jacoviano.
  petsc_call(MatDuplicate(milonga.R,MAT_DO_NOT_COPY_VALUES,&A));
  petsc_call(TSCreate(PETSC_COMM_WORLD,&milonga.ts)); //It is destroyed in allocate.c
  petsc_call(TSSetProblemType(milonga.ts,TS_LINEAR));
  petsc_call(TSSetType(milonga.ts,milonga.ts_type)); //It must be here because if not, it does not work.
  petsc_call(TSSetExactFinalTime(milonga.ts,TS_EXACTFINALTIME_MATCHSTEP));
  petsc_call(TSSetSolution(milonga.ts,milonga.phi));  //phi should have the initial condition.
  petsc_call(TSSetEquationType(milonga.ts,TS_EQ_IMPLICIT));
  petsc_call(TSSetIFunction(milonga.ts,NULL, f,&milonga));
//  petsc_call(TSSetRHSFunction(milonga.ts,NULL, derecha,&milonga));
  petsc_call(TSSetIJacobian(milonga.ts,A,A   , fjac, &milonga));
  petsc_call(TSSetPreStage(milonga.ts,evaluate_matrices));
//  petsc_call(TSSetTolerances(milonga.ts,1e-85,NULL ,1e-85,NULL));
  petsc_call(TSGetKSP(milonga.ts,&ksp));
  if(ksp != NULL)
    {
    petsc_call(KSPSetTolerances(ksp,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT));
//    petsc_call(KSPGetType(ksp,&kspname));
//    petsc_call(PetscPrintf(PETSC_COMM_WORLD,"%s\n",kspname));
    petsc_call(KSPSetType(ksp,KSPBCGS)); //It sets the ksp solver type.
    }

  petsc_call(TSGetSNES(milonga.ts,&snes));
  if(snes != NULL)
    {
    petsc_call(KSPSetTolerances(ksp,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT));
    petsc_call(SNESGetType(snes,&snesname));
//    petsc_call(PetscPrintf(PETSC_COMM_WORLD,"%s\n",snesname));
    }

//En algún lado tengo que destruir A.
  return WASORA_RUNTIME_OK;
  }
#undef  __FUNCT__
#define __FUNCT__ "milonga_solve_tra_slepc"
int milonga_solve_tra_slepc(Mat R, Mat F, Vec phi , PetscReal t,  PetscReal dt) {
//Ver punto 6.1.4, capas que se puede hacer más fácil.
  PetscReal      ftime,ptime,pasotiempo,atol,rtol,dtol;
  PetscInt its,fails,step,maxits;
  KSP ksp;
  KSPType kspname;
//  SNES snes;
//
//  petsc_call(TSGetSNES(milonga.ts,&snes));
//  if(snes==NULL)
//    printf("snes null\n");
//  petsc_call(SNESGetKSP(snes,&ksp));
//  petsc_call(KSPGetType(ksp,&kspname));
//  petsc_call(PetscPrintf(PETSC_COMM_WORLD,"snes %s\n",kspname));
//  petsc_call(TSGetKSP(milonga.ts,&ksp));
//  if(ksp==NULL)
//    printf("ksp null\n");
//  petsc_call(KSPGetType(ksp,&kspname));
//  petsc_call(PetscPrintf(PETSC_COMM_WORLD,"ksp %s\n",kspname));

  petsc_call(TSSetInitialTimeStep(milonga.ts,t-dt,dt)); 
  petsc_call(TSSetTimeStep(milonga.ts,dt));
//  petsc_call(TSMonitorSet(milonga.ts,monitor,NULL,NULL));  //Despues borrar.
/*
    Set a large number of timesteps and final duration time
*/
//  petsc_call(TSSetExactFinalTime(milonga.ts,TS_EXACTFINALTIME_MATCHSTEP));
  petsc_call(TSSetDuration(milonga.ts,200000,t)); 
  fails=2000;
  petsc_call(TSSetMaxSNESFailures(milonga.ts,fails));
  petsc_call(TSSetMaxStepRejections(milonga.ts,2000));
/*
    Set any additional options from the options database. This
   includes all options for the nonlinear and linear solvers used
   internally the timestepping routines.
*/
//  petsc_call(TSSetTolerances(milonga.ts,1e-20,NULL ,1e-20,NULL));
//  petsc_call(TSGetKSP(milonga.ts,&ksp));
//  petsc_call(KSPGetType(ksp,&kspname));
//  petsc_call(PetscPrintf(PETSC_COMM_WORLD,"%s\n",kspname));
//  petsc_call(KSPGetTolerances(ksp,&rtol,&atol,&dtol,&maxits));
//  petsc_call(PetscPrintf(PETSC_COMM_WORLD,"#maxits=%D rtol=%e atol=%e dtol=%e \n",maxits,(double)rtol,(double)atol,(double)dtol));
//  petsc_call(KSPSetTolerances(ksp,1e-25,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT));
  petsc_call(TSGetTolerances(milonga.ts,&atol,NULL,&rtol,NULL));
//  petsc_call(PetscPrintf(PETSC_COMM_WORLD,"#             rtol=%e atol=%e \n",(double)rtol,(double)atol));
  petsc_call(TSSetFromOptions(milonga.ts));
  petsc_call(TSSetUp(milonga.ts));
/*
    Perform the solve. This is where the timestepping takes place.
*/
  petsc_call(TSSolve(milonga.ts,milonga.phi));
  petsc_call(TSGetSolveTime(milonga.ts,&ftime));


/*
    Get the number of steps
*/
  petsc_call(TSGetTimeStepNumber(milonga.ts,&its));
  petsc_call(TSGetTimeStep(milonga.ts,&pasotiempo));

//  petsc_call(PetscPrintf(PETSC_COMM_WORLD,"#Number of timesteps = %D final time %4.2e\n",its,(double)ftime));
//  petsc_call(PetscPrintf(PETSC_COMM_WORLD,"#dt = %4.2e\n",(double)pasotiempo));
//  MatDestroy(&A);
  return WASORA_RUNTIME_OK;

}
static PetscErrorCode evaluate_matrices(TS ts,PetscReal stagetime )
  {
  PetscScalar       t0;
  t0=wasora_var(wasora_special_var(t)); //It saves the wasora time.
  wasora_var(wasora_special_var(t)) = stagetime;
  petsc_call(MatZeroEntries(milonga.R));
  petsc_call(MatZeroEntries(milonga.F));    
  petsc_call(MatZeroEntries(milonga.B));    
  wasora_call(milonga.matrices_build());  //This should build the matrices at time t.
  wasora_var(wasora_special_var(t))=t0;//It restores the wasora time.
  return 0;
  }
//This function is the F(t,u,u_dot) of manual.pdf.
static PetscErrorCode f(TS ts,PetscReal t, Vec u ,Vec u_t ,Vec F, void *ctx)
  {
//En milonga.c definí B*u_t+(R-F)*u=0  en milonga.R quedó R-F.
  PetscErrorCode    ierr;
  PetscScalar       *f,t0;
  const PetscScalar *x,*xdot;
  Vec aux;
  VecDuplicate(milonga.phi,&aux);
//  printf("tiempo= %g\n",t);
//  petsc_call(VecView(u_t, PETSC_VIEWER_STDOUT_WORLD));
//  VecCopy(milonga.phi,aux);
//  printf("wasora t= %e  t = %e \n",wasora_var(wasora_special_var(t)),t);
  petsc_call(MatAXPY(milonga.R, -1.0, milonga.F, SUBSET_NONZERO_PATTERN));  //R represents the A matrix of the chapter 6 of manual.pdf
  petsc_call(MatMult(milonga.B, u_t ,F));
  petsc_call(MatMult(milonga.R, u ,aux));
  petsc_call(VecAXPY(F,1.0,aux));
  petsc_call(VecDestroy(&aux));  //Lo destruyo por las dudas de que consuma toda la memoria.
//  printf("tiempo= %g\n",t);
//  petsc_call(VecView(F  , PETSC_VIEWER_STDOUT_WORLD));
//  PetscPrintf(PETSC_COMM_WORLD,"AAAAAAAAAAAA t=%4.2e\n",(double)t);
  return(0);
  }
//This function is the Jacovian of F. J=shift*B+(R-F)
static PetscErrorCode fjac(TS ts,PetscReal t,Vec u ,Vec u_t,PetscReal shift,Mat A,Mat B, void *ctx )
  {//In R is (R-F)
//Me fijé poniendo printf y vi que primero llama a f, así que no hace falta que las arme acá.
  MatCopy(milonga.R, A, DIFFERENT_NONZERO_PATTERN);
  MatAXPY(A,shift,milonga.B,DIFFERENT_NONZERO_PATTERN);
//  petsc_call(PetscPrintf(PETSC_COMM_WORLD,"shift = %4.2e\n",(double)shift));
//  PetscPrintf(PETSC_COMM_WORLD,"BBBBBBBBBBBB t=%4.2e\n",(double)t);
  return 0;
  }
static PetscErrorCode monitor(TS ts,PetscInt steps,PetscReal time, Vec u,void *mctx)
  {
  PetscReal dt;
  petsc_call(TSGetTimeStep(milonga.ts,&dt));
  //petsc_call(PetscPrintf(PETSC_COMM_WORLD,"#en monitor dt = %4.2e\n",(double)dt));
  printf("#en monitor dt = %4.2e\n",(double)dt);
  return 0;
  }
int amano(void)
  {
  Vec aux;
  PetscScalar t0;
  VecDuplicate(milonga.phi,&aux);
  t0=wasora_var(wasora_special_var(t)); //It saves the wasora time.
  wasora_var(wasora_special_var(t)) = wasora_var(wasora_special_var(t))-wasora_var(wasora_special_var(dt)) ;
  petsc_call(MatZeroEntries(milonga.R));
  petsc_call(MatZeroEntries(milonga.F));    
  petsc_call(MatZeroEntries(milonga.B));    
  wasora_call(milonga.matrices_build());  //This should build the matrices at time t.
  wasora_var(wasora_special_var(t))=t0;//It restores the wasora time.
  petsc_call(MatMult(milonga.B,milonga.phi,aux));
  petsc_call(VecScale(aux,1/wasora_var(wasora_special_var(dt)))) ;
  petsc_call(MatZeroEntries(milonga.R));
  petsc_call(MatZeroEntries(milonga.F));    
  petsc_call(MatZeroEntries(milonga.B));    
  wasora_call(milonga.matrices_build());  //This should build the matrices at time t.
  petsc_call(MatAXPY(milonga.R, -1.0, milonga.F, SUBSET_NONZERO_PATTERN));
  petsc_call(MatAXPY(milonga.R, 1.0/wasora_var(wasora_special_var(dt)), milonga.B, SUBSET_NONZERO_PATTERN));
  wasora_call(milonga_solve_linear_petsc(milonga.R, aux , milonga.phi));
  VecDestroy(&aux);
  return;
  }
int amano2(void)
  {
  Vec aux;
  PetscScalar t0;
  VecDuplicate(milonga.phi,&aux);
  t0=wasora_var(wasora_special_var(t)); //It saves the wasora time.
  petsc_call(MatZeroEntries(milonga.R));
  petsc_call(MatZeroEntries(milonga.F));    
  petsc_call(MatZeroEntries(milonga.B));    
  wasora_call(milonga.matrices_build());  //This should build the matrices at time t.
  wasora_var(wasora_special_var(t))=wasora_var(wasora_special_var(t))+wasora_var(wasora_special_var(dt));//It restores the wasora time.
  petsc_call(MatMult(milonga.B,milonga.phi,aux));
  petsc_call(VecScale(aux,1/wasora_var(wasora_special_var(dt)))) ;
  petsc_call(MatZeroEntries(milonga.R));
  petsc_call(MatZeroEntries(milonga.F));    
  petsc_call(MatZeroEntries(milonga.B));    
  wasora_call(milonga.matrices_build());  //This should build the matrices at time t.
  petsc_call(MatAXPY(milonga.R, -1.0, milonga.F, SUBSET_NONZERO_PATTERN));
  petsc_call(MatAXPY(milonga.R, 1.0/wasora_var(wasora_special_var(dt)), milonga.B, SUBSET_NONZERO_PATTERN));
  wasora_var(wasora_special_var(t))=t0;
  wasora_call(milonga_solve_linear_petsc(milonga.R, aux , milonga.phi));
  VecDestroy(&aux);
  return;
  }
//This function is the G(t,u)       of manual.pdf.
static PetscErrorCode derecha(TS ts,PetscReal t, Vec u ,Vec F, void *ctx)
  {
//En milonga.c definí B*u_t+(R-F)*u=0  en milonga.R quedó R-F.
  PetscErrorCode    ierr;
  PetscScalar       *f,t0;
  const PetscScalar *x,*xdot;
  Vec aux;
  VecDuplicate(milonga.phi,&aux);
  VecCopy(milonga.phi,aux);
  wasora_var(wasora_special_var(t)) = t;
  petsc_call(MatZeroEntries(milonga.R));
  petsc_call(MatZeroEntries(milonga.F));    
  petsc_call(MatZeroEntries(milonga.B));    
  wasora_call(milonga.matrices_build());  //This should build the matrices at time t.
  wasora_var(wasora_special_var(t))=t0;//It restores the wasora time.
  petsc_call(MatAXPY(milonga.R, -1.0, milonga.F, SUBSET_NONZERO_PATTERN));  //R represents the A matrix of the chapter 6 of manual.pdf
  petsc_call(MatScale(milonga.R,-1.0));
  petsc_call(MatMult(milonga.R, u ,aux));
  wasora_call(milonga_solve_linear_petsc(milonga.B, aux , F));
  petsc_call(VecDestroy(&aux));  //Lo destruyo por las dudas de que consuma toda la memoria.
//  PetscPrintf(PETSC_COMM_WORLD,"AAAAAAAAAAAA t=%4.2e\n",(double)t);
  return(0);
  }
