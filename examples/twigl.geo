// Macro to set a equal spaced point on a line in the xy plane at z=0.
//The input variables are:
//dirx: x direction and x distance between neighbour points.
//diry: y direction and y distance between neoghbour points.
//x0: initial point in x.
//y0: initial point in y.
//npoints: the number of points.
//The output variables are:
//listpoints[]: all the points number in the same order as the were created.
Macro PointLine
nlistp = #listpoints[];
For i In {0:npoints-1}
//  Printf("%g %g",nlistp,i);
  listpoints[] += {newp};
  Point(listpoints[i+nlistp]) = { x0 + i * dirx , y0 + i * diry , 0.0, lc};
//  Printf("%g",i);
EndFor
Return
lc =  1.0;
lado = 160.0;
Point(1) = {  0    , 0     , 0 , lc };
Point(2) = {  lado , 0     , 0 , lc };
Point(3) = {  lado , lado  , 0 , lc };
Point(4) = {  0    , lado  , 0 , lc };
Point(5) = {  24   , 24    , 0 , lc };
Point(6) = {  56   , 24    , 0 , lc };
Point(7) = { 104   , 24    , 0 , lc };
Point(8) = { 136   , 24    , 0 , lc };
Point(9) =  {  24   , 56    , 0 , lc };
Point(10) = {  56   , 56    , 0 , lc };
Point(11) = { 104   , 56    , 0 , lc };
Point(12) = { 136   , 56    , 0 , lc };
Point(13) = {  24   , 104   , 0 , lc };
Point(14) = {  56   , 104   , 0 , lc };
Point(15) = { 104   , 104   , 0 , lc };
Point(16) = { 136   , 104   , 0 , lc };
Point(17) = {  24   , 136   , 0 , lc };
Point(18) = {  56   , 136   , 0 , lc };
Point(19) = { 104   , 136   , 0 , lc };
Point(20) = { 136   , 136   , 0 , lc };
//Boundary.
Line(1) = { 1 , 2 };
Line(2) = { 2 , 3 };
Line(3) = { 3 , 4 };
Line(4) = { 4 , 1 };
//Fuels' boundary.
Line(5) = { 5 , 6 };
Line(6) = { 6 , 7 };
Line(7) = { 7 , 8 };
Line(8) = { 8 , 12 };
Line(9) = { 12 , 16 };
Line(10) = { 16 , 20 };
Line(11) = { 20 , 19 };
Line(12) = { 19 , 18 };
Line(13) = { 18 , 17 };
Line(14) = { 17 , 13 };
Line(15) = { 13 ,  9 };
Line(16) = {  9 ,  5 };
//Internal lines.
Line(17) = { 6  , 10 };
Line(18) = { 10 ,  9 };


Line(19) = { 10 , 14 };
Line(20) = { 14 , 13 };

Line(21) = { 14 , 18 };

Line(22) = {  7 , 11 };
Line(23) = { 11 , 10 };

Line(24) = { 11 , 15 };
Line(25) = { 15 , 14 };

Line(26) = { 15 , 19 };

Line(27) = { 12 , 11 };

Line(28) = { 16 , 15 };

l1 = newll;
Line Loop(l1) = { 1 , 2 , 3 , 4 };  
Physical Line("External") = { 1 , 2 , 3 , 4 };
l2 = newll;
Line Loop(l2) = { 5 , 6 , 7 , 8 , 9 , 10 , 11 , 12 , 13 , 14 , 15 , 16 };  
s1 = news;
Plane Surface(s1) = { l1 , l2 };
l1 = newll;
Line Loop(l1) = { -23 , 24 , 25 , -19 };  
s2 = news;
Plane Surface(s2) = { l1 };
Physical Surface("FUEL3") = { s1 , s2 };

l1 = newll;
Line Loop(l1) = {  5  , 17 , 18 , 16 };  
s1 = news;
Plane Surface(s1) = { l1 };
s2[] = {s1};
l1 = newll;
Line Loop(l1) = { -20 , 21 , 13 , 14 };  
s1 = news;
Plane Surface(s1) = { l1 };
s2[] += {s1};
l1 = newll;
Line Loop(l1) = {  7  ,  8 , 27 ,-22 };  
s1 = news;
Plane Surface(s1) = { l1 };
s2[] += {s1};
l1 = newll;
Line Loop(l1) = { -28 , 10 , 11 ,-26 };  
s1 = news;
Plane Surface(s1) = { l1 };
s2[] += {s1};
Physical Surface("FUEL1") = { s2[] };


l1 = newll;
Line Loop(l1) = { -18 , 19 , 20 , 15 };  
s1 = news;
Plane Surface(s1) = { l1 };
s2[] = {s1};
l1 = newll;
Line Loop(l1) = {  6  , 22 , 23 ,-17 };  
s1 = news;
Plane Surface(s1) = { l1 };
s2[] += {s1};
l1 = newll;
Line Loop(l1) = { -27 ,  9 , 28 ,-24 };  
s1 = news;
Plane Surface(s1) = { l1 };
s2[] += {s1};
l1 = newll;
Line Loop(l1) = { -25 , 26 , 12 ,-21 };  
s1 = news;
Plane Surface(s1) = { l1 };
s2[] += {s1};
Physical Surface("FUEL2") = { s2[] };

Geometry.Surfaces=1;
Mesh.Algorithm = 8;
//Mesh.ElementOrder = 2;
Mesh.RecombineAll = 1;
