// Macro to set a equal spaced point on a line in the xy plane at z=0.
//The input variables are:
//dirx: x direction and x distance between neighbour points.
//diry: y direction and y distance between neoghbour points.
//x0: initial point in x.
//y0: initial point in y.
//npoints: the number of points.
//The output variables are:
//listpoints[]: all the points number in the same order as the were created.
Macro PointLine
nlistp = #listpoints[];
For i In {0:npoints-1}
//  Printf("%g %g",nlistp,i);
  listpoints[] += {newp};
  Point(listpoints[i+nlistp]) = { x0 + i * dirx , y0 + i * diry , 0.0, lc};
//  Printf("%g",i);
EndFor
Return
lc = 15.0;
H = 15.0 ; 
Point(1) = {  0 , 0  , 0 , lc };
Point(2) = {  H , 0  , 0 , lc };
Point(3) = {  0 , H  , 0 , lc };
Point(4) = {  H , H  , 0 , lc };
Point(5) = { 9*H, 0  , 0 , lc };
Point(6) = {11*H, 0  , 0 , lc };
Point(7) = {11*H,11*H, 0 , lc };
Point(8) = {  0 ,11*H, 0 , lc };
npoints = 8;
dirx = H;
diry = 0;
x0 = 0;
y0 = 9*H;
listpoints = { 5 , 6 , 7 , 8 };
Call PointLine;
npoints = 2;
x0 = 7*H;
y0 = 8*H;
Call PointLine;
npoints = 2;
x0 = 8*H;
y0 = 7*H;
Call PointLine;
npoints = 6;
x0 = 9*H;
y0 = 6*H;
dirx = 0;
diry = -H;
Call PointLine;
listpoints2[] ={listpoints[]};

Line(1) = { 1 , 2 };
Line(2) = { 2 , 4 };
Line(3) = { 4 , 3 };
Line(4) = { 3 , 1 };
Line Loop(5) = { 1 , 2 , 3 , 4 };
Plane Surface(1) = {5};
aux[] = Boundary{Surface{1};};
surfs[] = {aux[3],aux[0]};
Physical Surface("FA01") = {1};
For j In {0:6}
  For i In {0:8}
    If ( (i!=0) || (j!=0) )
      newsup[] = Translate{ i*H , j*H , 0} {Duplicata{Surface{1}; }} ;
      v = Sprintf("FA%02g",i+9*j+1);
      Physical Surface(Str(v)) = {newsup[0]};
      If ( j==0 )
        aux[] = Boundary{Surface{newsup[0]};};
        surfs[] += {aux[0]};
      EndIf
      If ( i==0 )
        aux[] = Boundary{Surface{newsup[0]};};
        surfs[] = {aux[3],surfs[]};
      EndIf
    EndIf
  EndFor
EndFor
j = 7;
For i In {0:7}
  newsup[] = Translate{ i*H , j*H , 0} {Duplicata{Surface{1}; }} ;
  v = Sprintf("FA%02g",i+64);
  Physical Surface(Str(v)) = {newsup[0]};
  If (i==0)
    aux[] = Boundary{Surface{newsup[0]};};
    surfs[] = {aux[3],surfs[]};
  EndIf
EndFor
j = 8;
For i In {0:6}
  newsup[] = Translate{ i*H , j*H , 0} {Duplicata{Surface{1}; }} ;
  v = Sprintf("FA%02g",i+72);
  Physical Surface(Str(v)) = {newsup[0]};
  If (i==0)
    aux[] = Boundary{Surface{newsup[0]};};
    surfs[] = {aux[3],surfs[]};
  EndIf
EndFor
listlines[] = {};
For i In {0:#listpoints2[]-1}
//  Printf("%g %g",#listpoints2[]-1,i);
  l = newl;
  listlines[] += {l} ;
  If (i!=#listpoints2[]-1)
    Line(l) = {listpoints2[i] , listpoints2[i+1]};
  Else
    Line(l) = {listpoints2[i] , listpoints2[0]};
  EndIf
EndFor
l = newl;
//For i In {0:#listlines[]}
//  Printf("%g %g",i,listlines[i]);
//EndFor
Line Loop(l) = {listlines[]};
s = news;
Plane Surface(s) = {l};
Physical Surface("Reflector") = s;
surfs[] += {s};
frontera[] = Boundary{Surface{s};};
Physical Line("External") = {frontera[1] , frontera[2]};

fronref[] = {frontera[0] , surfs[], frontera[3]};
Physical Line("Reflected") = {fronref[]};
Geometry.Surfaces=1;
Coherence;
Mesh.Algorithm = 8;
//Mesh.ElementOrder = 2;
Mesh.RecombineAll = 1;
