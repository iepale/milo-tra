// Macro to set a equal spaced point on a line in the xy plane at z=0.
//The input variables are:
//dirx: x direction and x distance between neighbour points.
//diry: y direction and y distance between neoghbour points.
//x0: initial point in x.
//y0: initial point in y.
//npoints: the number of points.
//The output variables are:
//listpoints[]: all the points number in the same order as the were created.
Macro PointLine
nlistp = #listpoints[];
For i In {0:npoints-1}
//  Printf("%g %g",nlistp,i);
  listpoints[] += {newp};
  Point(listpoints[i+nlistp]) = { x0 + i * dirx , y0 + i * diry , z0 , lc};
//  Printf("%g",i);
EndFor
Return
lc = 15.0;
H = 15.0 ; 
Point(1) = {  0 , 0  , 30.0 , lc };
Point(2) = {  H , 0  , 30.0 , lc };
Point(3) = {  0 , H  , 30.0 , lc };
Point(4) = {  H , H  , 30.0 , lc };
Point(5) = { 9*H, 0  , 30.0 , lc };
Point(6) = {11*H, 0  , 30.0 , lc };
Point(7) = {11*H,11*H, 30.0 , lc };
Point(8) = {  0 ,11*H, 30.0 , lc };
npoints = 8;
dirx = H;
diry = 0;
x0 = 0;
y0 = 9*H;
z0 = 30.0;
listpoints = { 5 , 6 , 7 , 8 };
Call PointLine;
npoints = 2;
x0 = 7*H;
y0 = 8*H;
z0 = 30.0;
Call PointLine;
npoints = 2;
x0 = 8*H;
y0 = 7*H;
z0 = 30.0;
Call PointLine;
npoints = 6;
x0 = 9*H;
y0 = 6*H;
dirx = 0;
diry = -H;
z0 = 30.0;
Call PointLine;
listpoints2[] ={listpoints[]};

Line(1) = { 1 , 2 };
Line(2) = { 2 , 4 };
Line(3) = { 4 , 3 };
Line(4) = { 3 , 1 };
Line Loop(5) = { 1 , 2 , 3 , 4 };
Plane Surface(1) = {5};
aux[] = Boundary{Surface{1};};
surfs[] = {aux[3],aux[0]};
listPhySurf[] = {}; //It stores the physical surfaces which are going to be extruded.
//namePhySurf[] = Str{}; //It stores the name of the physical surfaces which are going to be extruded.
//Physical Surface("FA01") = {1};
listPhySurf[] += {1};
namePhySurf[] = Str("FA01");
Transfinite Line {1,2,3,4} = 2 Using Progression 1;
aux2[] = {};
For j In {0:6}
  For i In {0:8}
    If ( (i!=0) || (j!=0) )
      newsup[] = Translate{ i*H , j*H , 0} {Duplicata{Surface{1}; }} ;
      v = Sprintf("FA%02g",i+9*j+1);
//      Physical Surface(Str(v)) = {newsup[0]};
      listPhySurf[] += {newsup[]};
      namePhySurf[] += Str( v );
      If ( j==0 )
        aux[] = Boundary{Surface{newsup[0]};};
        surfs[] += {aux[0]};
      EndIf
      If ( i==0 )
        aux[] = Boundary{Surface{newsup[0]};};
        surfs[] = {aux[3],surfs[]};
      EndIf
    EndIf
  EndFor
EndFor
j = 7;
For i In {0:7}
  newsup[] = Translate{ i*H , j*H , 0} {Duplicata{Surface{1}; }} ;
  v = Sprintf("FA%02g",i+64);
//  Physical Surface(Str(v)) = {newsup[0]};
  namePhySurf[] += Str(v);
  listPhySurf[] += {newsup[]};
  If (i==0)
    aux[] = Boundary{Surface{newsup[0]};};
    surfs[] = {aux[3],surfs[]};
  EndIf
EndFor
j = 8;
For i In {0:6}
  newsup[] = Translate{ i*H , j*H , 0} {Duplicata{Surface{1}; }} ;
  v = Sprintf("FA%02g",i+72);
//  Physical Surface(Str(v)) = {newsup[0]};
  listPhySurf[] += {newsup[]};
  namePhySurf[] += Str( v );
  If (i==0)
    aux[] = Boundary{Surface{newsup[0]};};
    surfs[] = {aux[3],surfs[]};
  EndIf
EndFor
listlines[] = {};
For i In {0:#listpoints2[]-1}
//  Printf("%g %g",#listpoints2[]-1,i);
  l = newl;
  listlines[] += {l} ;
  If (i!=#listpoints2[]-1)
    Line(l) = {listpoints2[i] , listpoints2[i+1]};
  Else
    Line(l) = {listpoints2[i] , listpoints2[0]};
  EndIf
EndFor
l = newl;
//For i In {0:#listlines[]}
//  Printf("%g %g",i,listlines[i]);
//EndFor
Line Loop(l) = {listlines[]};
s = news;
Plane Surface(s) = {l};
//Physical Surface("Reflector") = s;
listPhySurf[] += {s};
namePhySurf[] += Str( "Reflector" );
surfs[] += {s};
frontera[] = Boundary{Surface{s};};
//Physical Line("External") = {frontera[1] , frontera[2]};

fronref[] = {frontera[0] , surfs[], frontera[3]};
//Physical Line("Reflected") = {fronref[]};

bottomSurf[] = {listPhySurf[]};
compref[] = {}; //Compund volumes
//Layers in active lenght:
//layers[] = {60,30,120,30,60};
NL = 5; //Number of layers in core extrusion.
layers[] = {300};
For j In {0:#layers[]-1}
  topsurf[] = {};
  For i In {0:#listPhySurf[]-1}
    vols[] = Extrude{ 0 , 0 , layers[j] } { 
      Surface{listPhySurf[i]} ; Recombine ; Layers{NL};
      } ;
  If (StrCmp(namePhySurf[i],"Reflector")!=0)
    Physical Volume(StrCat(namePhySurf[i],Sprintf("_%g",j))) = vols[1];
  Else
    compref[] += {vols[1]};
  EndIf
    topsurf[] += {vols[0]};
//    Printf("%g %g %g %g",vols[1],j,i,listPhySurf[i]);
  If (i==#listPhySurf[]-1) //Reflector
    external[] += {vols[3],vols[4]};
    reflected[] += {vols[2],vols[5]};
    aux2[] = Abs(Boundary{Surface{vols[3],vols[4]};});
    Transfinite Line {aux2[0],aux2[2],aux2[4],aux2[6]} = 12;
    aux2[] = Abs(Boundary{Surface{vols[2],vols[5]};});
    Transfinite Line {aux2[0],aux2[2],aux2[4],aux2[6]} = 3;
  EndIf
  If (StrCmp(namePhySurf[i],"FA01")==0)
    reflected[] += {vols[2],vols[5]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA02")==0)
    reflected[] += {vols[2]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA03")==0)
    reflected[] += {vols[2]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA04")==0)
    reflected[] += {vols[2]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA05")==0)
    reflected[] += {vols[2]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA06")==0)
    reflected[] += {vols[2]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA07")==0)
    reflected[] += {vols[2]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA08")==0)
    reflected[] += {vols[2]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA09")==0)
    reflected[] += {vols[2]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA10")==0)
    reflected[] += {vols[5]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA19")==0)
    reflected[] += {vols[5]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA28")==0)
    reflected[] += {vols[5]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA37")==0)
    reflected[] += {vols[5]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA46")==0)
    reflected[] += {vols[5]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA55")==0)
    reflected[] += {vols[5]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA64")==0)
    reflected[] += {vols[5]};
  EndIf
  If (StrCmp(namePhySurf[i],"FA72")==0)
    reflected[] += {vols[5]};
  EndIf
    If (i!=#listPhySurf[]-1) //Reflector
    aux2[] = Abs(Boundary{Volume{vols[1]};});
    For k In {0:#aux2[]-1}
      aux3[] = Abs(Boundary{Surface{aux2[k]};});
      Transfinite Line {aux3[]} = 2;
    EndFor
    EndIf
  EndFor  //Cierra i
  listPhySurf[] = {topsurf[]};
EndFor  //Cierra j

//Layer below active lenght:
ref[] = {};
For i In {0:#listPhySurf[]-1}
  vols[] = Extrude{ 0 , 0 , -30 } { 
    Surface{bottomSurf[i]} ; Recombine ;Layers{2};
    } ;
//  Physical Volume(StrCat(namePhySurf[i],Sprintf("_%g",j))) = vols[1];
  ref[] += {vols[1]};
  external[] += {vols[0]};
//    Printf("%g %g %g %g",vols[1],j,i,listPhySurf[i]);
If (i==#listPhySurf[]-1) //Reflector
  external[] += {vols[3],vols[4]};
  reflected[] += {vols[2],vols[5]};
    aux2[] = Abs(Boundary{Surface{vols[3],vols[4]};});
    Transfinite Line {aux2[0],aux2[2],aux2[4],aux2[6]} = 12;
  aux2[] = Abs(Boundary{Surface{vols[2],vols[5]};});
    Transfinite Line {aux2[0],aux2[2],aux2[4],aux2[6]} = 3;
EndIf
If (StrCmp(namePhySurf[i],"FA01")==0)
  reflected[] += {vols[2],vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA02")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA03")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA04")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA05")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA06")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA07")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA08")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA09")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA10")==0)
  reflected[] += {vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA19")==0)
  reflected[] += {vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA28")==0)
  reflected[] += {vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA37")==0)
  reflected[] += {vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA46")==0)
  reflected[] += {vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA55")==0)
  reflected[] += {vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA64")==0)
  reflected[] += {vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA72")==0)
  reflected[] += {vols[5]};
EndIf
//If (StrCmp(namePhySurf[i],"FA53")==0 || StrCmp(namePhySurf[i],"FA54")==0 || StrCmp(namePhySurf[i],"FA62")==0|| StrCmp(namePhySurf[i],"FA63")==0)
//  Physical Volume(StrCat(namePhySurf[i],Sprintf("CR"))) = vols[1];
//EndIf
    If (i!=#listPhySurf[]-1) //Reflector
      aux2[] = Abs(Boundary{Volume{vols[1]};});
      For k In {0:#aux2[]-1}
        aux3[] = Abs(Boundary{Surface{aux2[k]};});
        Transfinite Line {aux3[]} = 2;
      EndFor
    EndIf
EndFor

//Layer over active lenght:
For i In {0:#listPhySurf[]-1}
  vols[] = Extrude{ 0 , 0 ,  30 } { 
    Surface{topsurf[i]} ; Recombine ;Layers{2};
    } ;
//  Physical Volume(StrCat(namePhySurf[i],Sprintf("_%g",j))) = vols[1];
  ref[] += {vols[1]};
  external[] += {vols[0]};
//    Printf("%g %g %g %g",vols[1],j,i,listPhySurf[i]);
If (i==#listPhySurf[]-1) //Reflector
  external[] += {vols[3],vols[4]};
  reflected[] += {vols[2],vols[5]};
    aux2[] = Abs(Boundary{Surface{vols[3],vols[4]};});
    Transfinite Line {aux2[0],aux2[2],aux2[4],aux2[6]} = 12;
  aux2[] = Abs(Boundary{Surface{vols[2],vols[5]};});
    Transfinite Line {aux2[0],aux2[2],aux2[4],aux2[6]} = 3;
EndIf
If (StrCmp(namePhySurf[i],"FA01")==0)
  reflected[] += {vols[2],vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA02")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA03")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA04")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA05")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA06")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA07")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA08")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA09")==0)
  reflected[] += {vols[2]};
EndIf
If (StrCmp(namePhySurf[i],"FA10")==0)
  reflected[] += {vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA19")==0)
  reflected[] += {vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA28")==0)
  reflected[] += {vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA37")==0)
  reflected[] += {vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA46")==0)
  reflected[] += {vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA55")==0)
  reflected[] += {vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA64")==0)
  reflected[] += {vols[5]};
EndIf
If (StrCmp(namePhySurf[i],"FA72")==0)
  reflected[] += {vols[5]};
EndIf
    If (i!=#listPhySurf[]-1) //Reflector
      aux2[] = Abs(Boundary{Volume{vols[1]};});
      For k In {0:#aux2[]-1}
        aux3[] = Abs(Boundary{Surface{aux2[k]};});
        Transfinite Line {aux3[]} = 2;
      EndFor
    EndIf
EndFor

Physical Surface("External") = {external[]};
Physical Surface("Reflected") = {reflected[]};
//s = newv;
//Compound Volume(s) = {compref[],ref[]};
Physical Volume("Reflector") = {compref[],ref[]};

Geometry.Surfaces=1;
Coherence;
//Transfinite Surface "*";
Mesh.Algorithm = 8;
Mesh.RecombinationAlgorithm = 1; //Blossom.
Mesh.RecombineAll = 1;
Mesh.Recombine3DAll = 1;
//Mesh.ElementOrder = 2;
